using System;
using System.Collections.Generic;
using System.IO;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Text.RegularExpressions;

using sah.TabSaves.Cli.Infrastructure;


namespace sah.TabSaves.Cli
{
    public class FileEventFilter  : IObservable<IGroupedObservable<String,FileSystemEventArgs>>
    {

        private static readonly Regex _sessionRegex = new Regex($"(?<name>[^_~.]*).*");

        public FileEventFilter(params IObservable<FileSystemEventArgs>[] fileEvents)
        {
            InternalObservable = Observable.Merge(fileEvents).Where(fsea => !fsea.Name.StartsWith("DotNetZip-"))
                .Where(fsea => !fsea.Name.StartsWith("DotNetZip-"))
                .GroupBy(group);
        }

        private IObservable<IGroupedObservable<String,FileSystemEventArgs>> InternalObservable { get; set; }

        public IDisposable Subscribe(IObserver<IGroupedObservable<string, FileSystemEventArgs>> observer) => InternalObservable?.Subscribe(observer);

        private static String group(FileSystemEventArgs fsea)
        {
            Match m = _sessionRegex.Match(fsea.Name);
            if (m.Success)
            {
                return m.Groups["name"].Value;
            }
            return null;
        }
    }

    public static class FileEventFilterExtension
    {
        public static IObservable<IGroupedObservable<String, FileSystemEventArgs>> FileEventFilter(this IObservable<FileSystemEventArgs> xs, params IObservable<FileSystemEventArgs>[] ms )
        {
            List<IObservable<FileSystemEventArgs>> tmp = new List<IObservable<FileSystemEventArgs>>();
            tmp.Add(xs);
            if (ms != null && ms.Length > 0)
            {
                tmp.AddRange(ms);
            }

            return new FileEventFilter(tmp.ToArray()) ;
        }
    }
    
}