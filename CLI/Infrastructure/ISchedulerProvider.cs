using System.Reactive.Concurrency;

namespace sah.TabSaves.Cli.Infrastructure
{
    public interface ISchedulerProvider
    {
        IScheduler CurrentThread { get; }
        
        IScheduler Immediate { get; }
        IScheduler NewThread { get; }

        IScheduler TaskPool { get; }
        IScheduler ThreadPool { get; }
        
    }
}

