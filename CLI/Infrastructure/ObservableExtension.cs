using System;
using System.IO;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Reactive.Subjects;

namespace sah.TabSaves.Cli.Infrastructure
{
    public static class ObservableExtension
    {
        public static IObservable<T> Throttle<T>( this IObservable<T> obs, int count, Action<T> counterAction = null)
        {
            return Observable.Create<T>(o =>
            {
                //Repeat does not do what you may think (as I did). Repeat subscribes / unsubscribes the specfied number of time to the Observable
                var counter = obs.Skip(count).Take(1).Repeat().Publish();
                IDisposable counterActionSub = null;
                if( counterAction != null )
                {
                    counterActionSub = counter.Subscribe( t => counterAction(t));
                }
                var src = obs.Publish();
                var main = src.SkipUntil( counter).Take(1).Repeat().Subscribe(o.OnNext, o.OnError, o.OnCompleted);
                return new CompositeDisposable(counter.Connect(), src.Connect(), main, counterActionSub);
            });
            
        }

        public static IObservable<T> IntervalCountThrottle<T>(this IObservable<T> source, TimeSpan interval, int count)
        {
            return Observable.Create<T>(o =>
            {
                var timer = Observable.Timer(TimeSpan.Zero, interval).Publish();
                var src = source.Publish();
                var main = src.SkipUntil(timer).Take(count).Repeat().Subscribe(o.OnNext, o.OnError, o.OnCompleted);
                return new CompositeDisposable(timer.Connect(), src.Connect(), main);
            });
        }

        public static IObservable<T> Log<T>( this IObservable<T> obs, Func<T,String> toString)
        {
            return Observable.Create<T>(o =>
            {
                var src = obs.Publish();
                var passThrough = src;

                var logDis = src.Subscribe( on => Console.WriteLine( $"Log {toString(on)}"));
                var ptDis = passThrough.Subscribe(o.OnNext, o.OnError, o.OnCompleted);

                return new CompositeDisposable(src.Connect(), logDis, ptDis);
            });
            
        }

     
    }
}
