using System.Reactive.Concurrency;

namespace sah.TabSaves.Cli.Infrastructure
{
    public sealed class SchedulerProvider : ISchedulerProvider
    {
        public IScheduler CurrentThread
        {
            get { return Scheduler.CurrentThread; }
        }
        
        public IScheduler Immediate
        {
            get { return Scheduler.Immediate; }
        }
        public IScheduler NewThread
        {
            get { return NewThreadScheduler.Default;  }
        }

        public IScheduler TaskPool
        {
            get { return TaskPoolScheduler.Default;  }
        }

        public IScheduler ThreadPool
        {
            get { return ThreadPoolScheduler.Instance;  }
        }
        
    }
}