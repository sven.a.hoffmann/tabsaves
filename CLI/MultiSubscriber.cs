using System;
using System.IO;
using System.Reactive;
using System.Reactive.Concurrency;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Text.RegularExpressions;

using sah.TabSaves.Cli.Infrastructure;


namespace sah.TabSaves.Cli
{
    public static class MultiSubscriber
    {

        public static IDisposable SubscribeToAllKeys<K, T>(this IObservable<IGroupedObservable<K, T>> xs, IObserver<K> obs, IScheduler scheduler = null)
        {
            CompositeDisposable disposables = new CompositeDisposable();
            IDisposable dis = null;
            if( scheduler == null)
            {
                dis = xs.Subscribe(g =>
                {
                    var once = Observable.Timer(TimeSpan.Zero).Select(l => default(T));
                    var groupDis = g.Merge(once)
                                    .Sample(TimeSpan.FromMinutes(1))
                                    .Subscribe(n => obs.OnNext(g.Key));
                    disposables.Add(groupDis);
                });
            }
            else
            {
                dis = xs.Subscribe(g =>
                {
                    var once = Observable.Timer(TimeSpan.Zero, scheduler).Select(l => default(T));
                    var groupDis = g.Merge(once)
                                    .Sample(TimeSpan.FromMinutes(1), scheduler)
                                    .Subscribe(n => obs.OnNext(g.Key));
                    disposables.Add(groupDis);
                });
            }
            
            disposables.Add(dis);

            return disposables;
        }

        public static IDisposable SubscribeToAllKeys<K, T>(this IObservable<IGroupedObservable<K, T>> xs, Action<K> onNext, IScheduler scheduler = null)
        {
            return xs.SubscribeToAllKeys(Observer.Create(onNext), scheduler);
        }

    }


}