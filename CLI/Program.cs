﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reactive;
using System.Reactive.Concurrency;

using System.Reactive.Disposables;

using System.Reactive.Linq;
using System.Text.RegularExpressions;

using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace sah.TabSaves.Cli
{

    public static class Program
    {

        public static readonly String _tabBase = null;
        public static readonly String _tabSaves = null;
        public static readonly String _tabBackups = null;

        private static readonly Regex _sessionRegex = new Regex($"(?<name>[^_~.]*).*");

        static Program()
        {
            _tabBase = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            _tabSaves = _tabBase + @"\Saves";
            _tabBackups = _tabBase + @"\Backups";
        }


        static void Main(string[] args)
        {
            if(!Directory.Exists(_tabSaves))
            {
                Console.WriteLine($"Directory {_tabSaves} does not exist");
                return;
            }

            FileSystemWatcher fsw = new FileSystemWatcher(_tabSaves);

            var fileCreatedEvents = Observable.FromEvent<FileSystemEventHandler, FileSystemEventArgs>(handler =>
            {
                FileSystemEventHandler fsHandler = (sender, e) =>
                {
                    handler(e);
                };

                return fsHandler;
            },
            fsHandler => fsw.Created += fsHandler,
            fsHandler => fsw.Created -= fsHandler);

            var fileChangedEvents = Observable.FromEvent<FileSystemEventHandler, FileSystemEventArgs>(handler =>
            {
                FileSystemEventHandler fsHandler = (sender, e) =>
                {
                    handler(e);
                };

                return fsHandler;
            },
            fsHandler => fsw.Changed += fsHandler,
            fsHandler => fsw.Changed -= fsHandler);

            using IDisposable merged = fileChangedEvents.FileEventFilter(fileCreatedEvents).SubscribeToAllKeys(key => createBackups(key));
            fsw.EnableRaisingEvents = true;

            Console.WriteLine($"Started - Watching {fsw.Path}");
            Console.ReadLine();
        }

        private static void createBackups(String game)
        {
            Console.WriteLine($"creating save game backups - {DateTime.Now} - {game}");

            var files = Directory.GetFiles(_tabSaves, $"{game}*.*");
            if (files?.Length > 0)
            {
                if (!Directory.Exists(_tabBackups))
                {
                    Directory.CreateDirectory(_tabBackups);
                }

                String gameDir = $@"{_tabBackups}\{game}";
                if (!Directory.Exists(gameDir))
                {
                    Directory.CreateDirectory(gameDir);
                }
                var existingBackups = Directory.GetDirectories(gameDir);

                String backupBaseDir = gameDir + @"\B-";
                int max = 0;
                string lastBackupDir = backupBaseDir;
                if (existingBackups?.Length > 0)
                {
                    Regex regex = new Regex($"B-(?<count>\\d*)");
                    var latest = from d in existingBackups
                                 let c = Int32.Parse(regex.Match(d).Groups["count"].Value)
                                 orderby c descending
                                 select c;
                    max = latest.First();
                    lastBackupDir += $"{max}";
                }

                if (max == 0 || didSthChange(files, lastBackupDir))
                {
                    String newBackupDir = backupBaseDir + $"{++max}";
                    Console.WriteLine($"new backup - {newBackupDir}");

                    Directory.CreateDirectory(newBackupDir);
                    foreach (String f in files)
                    {
                        FileInfo fi = new FileInfo(f);
                        File.Copy(f, $@"{newBackupDir}\{fi.Name}");
                    }
                }
                else
                {
                    Console.WriteLine($"backup stopped, nothing changed - {game}");
                }
            }
        }

        private static bool didSthChange(string[] newFiles, String lastBackupDir)
        {
            DirectoryInfo di = new DirectoryInfo(lastBackupDir);
            var oldFiles = di.GetFileSystemInfos();
            if (oldFiles.Length != newFiles.Length)
                return true;

            var t = from o in oldFiles
                    from fi in (from n in newFiles select new FileInfo(n))
                    where o.Name == fi.Name
                    select new { Name = o.Name, Changed = (fi.LastWriteTimeUtc != o.LastWriteTimeUtc) };

            // Console.WriteLine($"{t.Count()} - {oldFiles.Length}");

            if (t.Count() != newFiles.Length)
                return true;

            return t.Any(a => a.Changed);
        }
    }
}

