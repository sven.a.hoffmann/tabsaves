# Purpose
Automatically creates save game backups of They Are Billions save games. TAB knows only the Iron Men mode which can be annoying when you learn the game or just want to try different things to get used to the game mechanics (especially the noise system). The concept can be applied to any game, just adjust the watched folder.

# Technology
- .NET Core 3.1
- Reactive Extensions
- XUnit to test Reactive Extensions

## Lessons learned
- Setup multi project workspace with VS Code
- Test Rx with XUnit
- Use .NET Test Explorer in VS Code
