using System.IO;
using System.Reactive;
using Microsoft.Reactive.Testing;
using Xunit;
using Xunit.Abstractions;

using sah.TabSaves.Cli;
using sah.TabSaves.Cli.Infrastructure;

using Emitted = System.Reactive.Linq.IGroupedObservable<string, System.IO.FileSystemEventArgs>;


namespace sah.TabSaves.Tests
{
    public class FileEventFilterTests : ReactiveTest 
    {
        private readonly ITestOutputHelper _output;

        public static ITestableObservable<FileSystemEventArgs> CreateTestData(TestScheduler t, int dataSetId)
        {
            switch (dataSetId)
            {
                case 1:
                    return t.CreateColdObservable( 
                                OnNext(210,new FileSystemEventArgs(WatcherChangeTypes.Changed, @"D:\testdir\", "Game1.zsav")),
                                OnNext(220,new FileSystemEventArgs(WatcherChangeTypes.Changed, @"D:\testdir\", "DotNetZip-")),
                                OnNext(230,new FileSystemEventArgs(WatcherChangeTypes.Changed, @"D:\testdir\", "Game1.zsav")),
                                OnNext(240,new FileSystemEventArgs(WatcherChangeTypes.Changed, @"D:\testdir\", "Game1.zsav")),
                                OnNext(240,new FileSystemEventArgs(WatcherChangeTypes.Changed, @"D:\testdir\", "Game2.zsav")),
                                OnNext(240,new FileSystemEventArgs(WatcherChangeTypes.Changed, @"D:\testdir\", "Game2.zsav"))
                                );
                case 2: 
                    return t.CreateColdObservable( 
                                OnNext(210,new FileSystemEventArgs(WatcherChangeTypes.Changed, @"D:\testdir\", "Game1.zsav")),
                                OnNext(220,new FileSystemEventArgs(WatcherChangeTypes.Changed, @"D:\testdir\", "DotNetZip-")),
                                OnNext(230,new FileSystemEventArgs(WatcherChangeTypes.Changed, @"D:\testdir\", "Game1.zsav")),
                                OnNext(240,new FileSystemEventArgs(WatcherChangeTypes.Changed, @"D:\testdir\", "Game1.zsav")),
                                OnNext(240,new FileSystemEventArgs(WatcherChangeTypes.Changed, @"D:\testdir\", "Game2.zsav")),
                                OnNext(240,new FileSystemEventArgs(WatcherChangeTypes.Changed, @"D:\testdir\", "Game2.zsav")),
                                OnNext(888,new FileSystemEventArgs(WatcherChangeTypes.Changed, @"D:\testdir\", "Game2.zsav")),
                                OnNext(999,new FileSystemEventArgs(WatcherChangeTypes.Changed, @"D:\testdir\", "Game2.zsav"))
                                );
                default:
                    return null;
            }
        }

        public FileEventFilterTests(ITestOutputHelper output)
        {
            _output = output;
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        public void Groups2_Filter(int dataSetId)
        {
            TestScheduler t = new TestScheduler();
            var xs = CreateTestData(t, dataSetId);

            // xs.Subscribe( onNext => _output.WriteLine( $"xs - {onNext.Name}"));
            _output.WriteLine( $"started: {t.Clock} - dataSetId: {dataSetId}");
            var testObs = t.Start(() => new FileEventFilter(xs), 0, 0, 1000);
            
            _output.WriteLine( testObs.Messages.Count.ToString());
            Assert.Equal(2, testObs.Messages.Count);
            testObs.Messages.AssertEqual<Recorded< Notification<Emitted>>>(
                    OnNext< Emitted>(211, g => g.Key == "Game1"),
                    OnNext<Emitted>(241, g => g.Key == "Game2")
                    );
            foreach ( var m in testObs.Messages)
            {
                _output.WriteLine($"{m.Time} - {m.Value.Kind} - {m.Value.Value.Key}");
            }
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        public void Groups_Filter_Extension(int dataSetId)
        {
            TestScheduler t = new TestScheduler();
            var xs = CreateTestData(t, dataSetId);
            _output.WriteLine($"started: {t.Clock} - dataSetId: {dataSetId}");
            var testObs = t.Start(() => xs.FileEventFilter(), 0, 0, 1000);

            Assert.Equal(2, testObs.Messages.Count);
            testObs.Messages.AssertEqual<Recorded<Notification<Emitted>>>(
                    OnNext<Emitted>(211, g => g.Key == "Game1"),
                    OnNext<Emitted>(241, g => g.Key == "Game2")
                    );
        }


    }

        
}