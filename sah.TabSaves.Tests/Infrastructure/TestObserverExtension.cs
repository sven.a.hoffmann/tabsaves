﻿using Microsoft.Reactive.Testing;
using System;
using System.Collections.Generic;
using System.Reactive;
using System.Text;
using System.Linq;

namespace sah.TabSaves.Tests.Infrastructure
{
    public static class TestObserverExtension
    {

        public static bool AssertAnyMessage<T> ( this ITestableObserver<T> to, Func<Recorded<Notification<T>>, bool> testPredicate)
        {
            bool rslt = to.Messages.Any(testPredicate);
            if (!rslt)
            {
                throw new Xunit.Sdk.NotSameException();
            }
            return true;
        }
    }
}
