using System;
using System.IO;
using System.Linq;
using System.Reactive;
using System.Reactive.Linq;
using Xunit;
using Xunit.Abstractions;

using Microsoft.Reactive.Testing;

using sah.TabSaves.Cli;
using sah.TabSaves.Tests.Infrastructure;


namespace sah.TabSaves.Tests
{
    public class MultiSubscriberTests : ReactiveTest 
    {
        private readonly ITestOutputHelper _output;

        public static ITestableObservable<FileSystemEventArgs> CreateTestData(TestScheduler t, int dataSetId)
        {
            switch (dataSetId)
            {
                case 1:
                    return t.CreateColdObservable( 
                                OnNext(210,new FileSystemEventArgs(WatcherChangeTypes.Changed, @"D:\testdir\", "Game1")),
                                OnNext(240,new FileSystemEventArgs(WatcherChangeTypes.Changed, @"D:\testdir\", "Game2"))
                                );
                case 2: 
                    return t.CreateColdObservable( 
                                OnNext(210,new FileSystemEventArgs(WatcherChangeTypes.Changed, @"D:\testdir\", "Game1")),
                                OnNext(230,new FileSystemEventArgs(WatcherChangeTypes.Changed, @"D:\testdir\", "Game1")),
                                OnNext(240,new FileSystemEventArgs(WatcherChangeTypes.Changed, @"D:\testdir\", "Game1")),
                                OnNext(240,new FileSystemEventArgs(WatcherChangeTypes.Changed, @"D:\testdir\", "Game2")),
                                OnNext(240,new FileSystemEventArgs(WatcherChangeTypes.Changed, @"D:\testdir\", "Game2")),
                                OnNext(888,new FileSystemEventArgs(WatcherChangeTypes.Changed, @"D:\testdir\", "Game2")),
                                OnNext(999,new FileSystemEventArgs(WatcherChangeTypes.Changed, @"D:\testdir\", "Game2"))
                                );
                case 3:
                    return t.CreateColdObservable(
                                OnNext(210, new FileSystemEventArgs(WatcherChangeTypes.Changed, @"D:\testdir\", "Game1")),
                                OnNext(230, new FileSystemEventArgs(WatcherChangeTypes.Changed, @"D:\testdir\", "Game1")),
                                OnNext(240, new FileSystemEventArgs(WatcherChangeTypes.Changed, @"D:\testdir\", "Game1")),
                                OnNext(240, new FileSystemEventArgs(WatcherChangeTypes.Changed, @"D:\testdir\", "Game2")),
                                OnNext(240, new FileSystemEventArgs(WatcherChangeTypes.Changed, @"D:\testdir\", "Game2")),
                                OnNext(888, new FileSystemEventArgs(WatcherChangeTypes.Changed, @"D:\testdir\", "Game2")),
                                OnNext(999, new FileSystemEventArgs(WatcherChangeTypes.Changed, @"D:\testdir\", "Game2")),
                                OnNext(240+TimeSpan.FromMinutes(1).Ticks+1, new FileSystemEventArgs(WatcherChangeTypes.Changed, @"D:\testdir\", "Game2"))
                                );
                default:
                    return null;
            }
        }

        public MultiSubscriberTests(ITestOutputHelper output)
        {
            _output = output;
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        public void SubscribeToAllKeys_OnlyOneEmission(int dataSetId)
        {
            TestScheduler t = new TestScheduler();
            var xs = CreateTestData(t, dataSetId);

            _output.WriteLine( $"started: {t.Clock} - dataSetId: {dataSetId}");
            var testObs =  t.CreateObserver<string>();
            using IDisposable subscription = xs.GroupBy(fsea => fsea.Name).SubscribeToAllKeys(testObs, t);
            t.AdvanceTo(TimeSpan.FromMinutes(2).Ticks);
            _output.WriteLine($"ended: {t.Clock} - dataSetId: {dataSetId}");

            //_output.WriteLine( testObs.Messages.Count.ToString());
            Assert.Equal(2, testObs.Messages.Count);
            testObs.AssertAnyMessage(rn => rn.Time > TimeSpan.FromMinutes(1).Ticks && rn.Value.Kind == NotificationKind.OnNext && rn.Value.Value == "Game1");
            testObs.AssertAnyMessage(rn => rn.Time > TimeSpan.FromMinutes(1).Ticks && rn.Value.Kind == NotificationKind.OnNext && rn.Value.Value == "Game2");
            //foreach (var m in testObs.Messages)
            //{
            //    _output.WriteLine($"{m.Time} - {m.Value.Kind} - {m.Value.Value}");
            //}
        }

        [Theory]
        [InlineData(3)]
        public void SubscribeToAllKeys_ResumeAfterOneMinute(int dataSetId)
        {
            TestScheduler t = new TestScheduler();
            var xs = CreateTestData(t, dataSetId);

            _output.WriteLine($"started: {t.Clock} - dataSetId: {dataSetId}");
            var testObs = t.CreateObserver<string>();
            using IDisposable subscription = xs.GroupBy(fsea => fsea.Name).SubscribeToAllKeys(testObs, t);
            t.AdvanceTo(TimeSpan.FromMinutes(3).Ticks);
            _output.WriteLine($"ended: {t.Clock} - dataSetId: {dataSetId}");

            //_output.WriteLine( testObs.Messages.Count.ToString());
            Assert.Equal(3, testObs.Messages.Count);
            testObs.AssertAnyMessage(rn => rn.Time > TimeSpan.FromMinutes(1).Ticks && rn.Value.Kind == NotificationKind.OnNext && rn.Value.Value == "Game1");
            testObs.AssertAnyMessage(rn => rn.Time > TimeSpan.FromMinutes(1).Ticks && rn.Value.Kind == NotificationKind.OnNext && rn.Value.Value == "Game2");
            testObs.AssertAnyMessage(rn => rn.Time > TimeSpan.FromMinutes(2).Ticks && rn.Value.Kind == NotificationKind.OnNext && rn.Value.Value == "Game2");
            //foreach (var m in testObs.Messages)
            //{
            //    _output.WriteLine($"{m.Time} - {m.Value.Kind} - {m.Value.Value}");
            //}
        }


    }

        
}