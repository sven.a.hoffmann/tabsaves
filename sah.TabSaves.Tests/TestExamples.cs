using System;
using System.Collections.Generic;

using System.IO;

using System.Reactive.Concurrency;
using System.Reactive.Linq;

using Microsoft.Reactive.Testing;

using Xunit;
using Xunit.Abstractions;


namespace sah.TabSaves.Tests
{
    public class TestExamples
    {
        private readonly ITestOutputHelper _output;

        public TestExamples(ITestOutputHelper output)
        {
            _output = output;
        }

        [Fact]
        public void IntervalTest()
        {
            var expectedValues = new long[] { 0, 1, 2, 3, 4 };
            var actualValues = new List<long>();
            var scheduler = new TestScheduler();
            var interval = Observable
                    .Interval(TimeSpan.FromSeconds(1), scheduler)
                    .Take(5);
            interval.Subscribe(actualValues.Add);
            scheduler.Start();

            _output.WriteLine(actualValues.Count.ToString());
        }

        [Fact]
        public void ScheduleTest()
        {
            TestScheduler t = new TestScheduler();
            // t.Schedule<Object>(  null, (sc, st) => { _output.WriteLine("ScheduleTest"); return null;} );
            t.Schedule(() => _output.WriteLine("ScheduleTest"));
            t.Start();
        }

        [Fact]
        public void CreateColdObservableTest()
        {
            TestScheduler t = new TestScheduler();
            var xs = t.CreateColdObservable(
                new Recorded<System.Reactive.Notification<String>>(10, System.Reactive.Notification.CreateOnNext("1")),
                new Recorded<System.Reactive.Notification<String>>(20, System.Reactive.Notification.CreateOnNext("2")),
                new Recorded<System.Reactive.Notification<String>>(30, System.Reactive.Notification.CreateOnNext("3")),
                new Recorded<System.Reactive.Notification<String>>(40, System.Reactive.Notification.CreateOnNext("4"))
                ).Publish();

            int called = 0;
            xs
                .Sample(TimeSpan.FromMinutes(1), t)
                .Subscribe(str => called++);

            int counter = 0;
            xs
                .Subscribe(str => counter++);
            xs.Connect();

            t.AdvanceTo(30);
            Assert.Equal(3, counter);

            t.AdvanceTo(40);
            Assert.Equal(4, counter);
            Assert.Equal(0, called);

            t.AdvanceTo(TimeSpan.FromMinutes(1).Ticks);

            Assert.Equal(1, called);
        }

        [Fact]
        public void FileSystemEventArgsTest()
        {
            TestScheduler t = new TestScheduler();
            var xs = t.CreateColdObservable(
                new Recorded<System.Reactive.Notification<FileSystemEventArgs>>(10, System.Reactive.Notification.CreateOnNext(new FileSystemEventArgs(WatcherChangeTypes.Changed, @"D:\testdir\", "afile.txt"))),
                new Recorded<System.Reactive.Notification<FileSystemEventArgs>>(20, System.Reactive.Notification.CreateOnNext(new FileSystemEventArgs(WatcherChangeTypes.Changed, @"D:\testdir\", "DotNetZip-"))),
                new Recorded<System.Reactive.Notification<FileSystemEventArgs>>(30, System.Reactive.Notification.CreateOnNext(new FileSystemEventArgs(WatcherChangeTypes.Changed, @"D:\testdir\", "afile.txt"))),
                new Recorded<System.Reactive.Notification<FileSystemEventArgs>>(40, System.Reactive.Notification.CreateOnNext(new FileSystemEventArgs(WatcherChangeTypes.Changed, @"D:\testdir\", "afile.txt")))
                );

            int counter = 0;
            xs
                // .Sample(TimeSpan.FromMinutes(1), t)
                .Where(fsea => !fsea.Name.StartsWith("DotNetZip-"))
                .Subscribe(str => counter++);

            t.AdvanceTo(TimeSpan.FromMinutes(1).Ticks);

            Assert.Equal(3, counter);
        }

        [Fact]
        //s. https://devblogs.microsoft.com/premier-developer/the-in-modifier-and-the-readonly-structs-in-c/
        public void ReadonlyValueTypeTest()
        {
            var l = new List<int> { 1, 2 };
            var roe = new ReadOnlyEnumerator(l);
            int rslt = roe.GetFirst();
            _output.WriteLine(rslt.ToString());// output: 0 because of defensive copies

            ref int r2 = ref roe.GetFirstRef();
            r2 = 5;

            Assert.Equal(2, l[1]);//no change in original list as List<int>.Current returns an int and a copy is made on assignment
            Assert.Equal( 5, roe._current);//change applied in ROE as GetFirstRef return a reference to the value type int variable "_current"
        }

        public class ReadOnlyEnumerator
        {
            private readonly List<int>.Enumerator _enumerator; //readonly modifier on mutable struct is a killer!
            public int _current;

            public ReadOnlyEnumerator(List<int> list)
            {
                _enumerator = list.GetEnumerator();
            }

            public int GetFirst()
            {
                _enumerator.MoveNext();
                return _enumerator.Current;
            }

            public ref int GetFirstRef()
            {
                _enumerator.MoveNext();
                _current = _enumerator.Current;
                return ref _current;
            }

        }

        [Fact]
        public void RefReturnTest()
        {
            var sut = new SomeClass() { _myField = 3};
            _output.WriteLine(sut.MyField.ToString());

            ref int test = ref sut.MyField;
            test = 5;

            Assert.Equal(5, sut._myField);
        }

        private class SomeClass
        {
            public int _myField;

            public ref int MyField
            {
                get
                {
                    return ref _myField;
                }
            }
            public ref int RefInt()
            {
                return ref _myField;
            }
        }

        // [Theory]
        // [MemberData(nameof(TestDataGenerator.GetPersonFromDataGenerator), MemberType = typeof(TestDataGenerator))]  

    }
}